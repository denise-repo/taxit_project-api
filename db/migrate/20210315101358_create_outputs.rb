class CreateOutputs < ActiveRecord::Migration[6.0]
  def change
    create_table :outputs do |t|
      t.string :file_name
      t.string :file_type

      t.timestamps
    end
  end
end
