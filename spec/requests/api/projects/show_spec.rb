# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  let(:project) { create(:project) }
  let(:project_1) { create_list(:project, 2) }

  context "GET show" do
    it 'has a 200 status code' do
      get :show, params: { id: project.to_param }

      expect(response).to have_http_status(:ok)
    end

    it 'returns the correct project' do
      get :show, params: { id: project.id }

      expect(JSON.parse(response.body)["id"]).to eq(project.id)
    end

    it 'return list of projects' do
      get :show, params: { id: project.id }

      expect(project_1.count).to eq(2)
    end
  end

end
