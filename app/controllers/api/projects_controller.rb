module Api
  class ProjectsController < ApplicationController

    def index
      projects = Project.all

      render status: :ok,
             json: projects
    end

    def show
      project = Project.find(id)

      render status: :ok,
             json: ProjectSerializer.new(project).serialize
    end

    # POST/project
    def create
      project = Project.new(creation_params)

      if project.save
        render status: :ok,
               json: ProjectSerializer.new(project).serialize
      else
        Error.create(error_message: project.errors.messages, resource: 'project')

        render status: :unprocessable_entity,
               json: project.errors
      end
    end

    #PUT /project/:id
    def update
      project = Project.find(id)

      if project.update!(creation_params)

        render status: :ok,
               json: ProjectSerializer.new(project).serialize
      else
        render status: :unprocessable_entity,
               json: project.errors
      end
    end

    # DELETE project/:id
    def destroy
      project = Project.find(id)

      if project.destroy
        render status: :ok,
               json: {message: 'Utilizador apagado com sucesso'}
      else
        render status: :unprocessable_entity,
               json: project.errors
      end
    end

    def create_user_project
      project = Project.find(id)

      user = User.find(permitted_params[:user_id])

      UserProject.find_or_create_by(user_id: user.id, project_id: project.id,
                                     project_name: project.name, user_name: user.first_name)

      project_details = Project.joins(:user_projects).where(user_projects: {user: user})
      render status: :ok,
             json: project_details
    end

    def delete_user_project
      project = Project.find(id)

      user = User.find(permitted_params[:user_id])

      user_proj = UserProject.find_or_create_by(user_id: user.id, project_id: project.id,
                                    project_name: project.name, user_name: user.first_name)

      if user_proj.destroy
        render status: :ok,
               json: {message: 'Utilizador apagado com sucesso'}
      else
        render status: :unprocessable_entity,
               json: user_proj.errors
      end
    end

    private

    def permitted_params
      params.permit(:id, :user_id, project: {})
    end

    def id
      permitted_params[:id]
    end

    def creation_params
      permitted_params[:project]
          .permit(:name, :start_date, :end_date, :status)
    end
  end
end
