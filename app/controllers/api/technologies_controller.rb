module Api
  class TechnologiesController < ApplicationController

    def index
      technologies = Technology.all

      render status: :ok,
             json: technologies
    end

    def show
      technology = Technology.find(id)

      render status: :ok,
             json: TechnologySerializer.new(technology).serialize
    end

    def create
      technology = Technology.new(creation_params)

      if technology.save
        render status: :ok,
               json: TechnologySerializer.new(technology).serialize
      else
        render status: :unprocessable_entity,
               json: technology.errors
      end
    end

    def update
      technology = Technology.find(id)

      if technology.update!(creation_params)

        render status: :ok,
               json: TechnologySerializer.new(technology).serialize
      else
        render status: :unprocessable_entity,
               json: project.errors
      end
    end


    def destroy
      technology = Technology.find(id)

      if technology.destroy
        render status: :ok,
               json: { message: 'Tecnologia apagada com sucesso' }
      else
        render status: :unprocessable_entity,
               json: technology.errors
      end
    end

    private

    def permitted_params
      params.permit(:id, technology: {})
    end

    def id
      permitted_params[:id]
    end

    def creation_params
      permitted_params[:technology].permit(:name)
    end
  end
end


