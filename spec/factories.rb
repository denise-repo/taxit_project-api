# frozen_string_literal: true

require 'json'

FactoryBot.define do
  factory :output do
    
  end
  factory :user do
    first_name { 'user_1' }
    last_name { 'user_1_manel' }
    phone { '911824332' }
    rank { 'some_rank' }
    entry_date { DateTime.now }
    sequence(:email) { |n| "user_#{n}@mail.com" }
  end

  factory :project do
    name { 'Project 1' }
    start_date { DateTime.now - 50 }
    end_date { nil }
    status { true }
  end

  factory :user_project do
    user
    project
    user_id {user.id}
    project_id {project.id}
    project_name { project.name }
    user_name { user.first_name }
  end
end
