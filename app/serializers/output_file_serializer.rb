# frozen_string_literal: true

require 'json'

class OutputFileSerializer
  def initialize(record)
    @record = record
  end

  def serialize
    {
      id: @record.id,
      file_name: @record.file_name,
      file_type: @record.file_type
    }
  end
end
