# frozen_string_literal: true

require 'json'

class ProjectSerializer
  def initialize(collection)
    @collection = collection
  end

  def serialize
    {
      id: @collection.id,
      name: @collection.name,
      start_date: @collection.start_date,
      end_date: @collection.end_date,
      status: @collection.status
    }
  end

  def serialize_user_projects
    @collection.map do |record|
      {
        id: record.id,
        name: record.name,
        start_date: record.start_date,
        end_date: record.end_date,
        status: record.status
      }
    end
  end
end


