# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Projects API', type: :request do
  let(:endpoint) do
    "/api/projects/#{project.id}/user/#{user.id}/create_user_project"
  end
  let(:project) { create(:project) }
  let(:user) { create(:user) }
  let(:user_project) { create(:user_project) }

  let(:permited_params) do
    { user: { user_id: user.id },
      project: { name: 'Project 3000',
                 start_date: (DateTime.now - 365) } }
  end

  before do
    user_project
  end

  describe 'create project from specific user' do
    it 'create project' do
      post endpoint

      expect(response).to have_http_status(:ok)
    end
  end
end
