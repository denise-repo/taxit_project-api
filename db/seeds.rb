# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Users seeds
user_1 = User.create!(first_name: 'User1', last_name: 'Machado',
             email: 'demachado@deloitte.pt', phone: '911822222',
             rank: 'programmer')

user_2 = User.create!(first_name: 'JoséDosTestes', last_name: 'Lopes',
             email: 'jl@deloitte.pt', phone: '911822232',
             rank: 'oculista')


#Projects seeds
project_1 = Project.create!(name: 'Projecto 1', start_date: '01-01-2016', status: 1)

project_2 = Project.create!(name: 'Projecto 2', start_date: '01-04-2018', end_date: '10-06-2020', status: 0)

Project.create!(name: 'Projecto 3', start_date: '01-01-2013', status: 1)


#UserProjects seeds

UserProject.create!(user_id: user_1.id, project_id: project_1.id,
                    project_name: project_1.name, user_name: user_1.first_name)

#UserProject.create!(user_id: user_1.id, project_id: project_2.id, project_name: project_2.name, user_name: user_1.first_name)

UserProject.create!(user_id: user_2.id, project_id: project_1.id,
                    project_name: project_1.name, user_name: user_2.first_name)
