class Project < ApplicationRecord
  scope :active, -> { where(end_date: nil) }
  scope :inactive, -> { where("end_date is not null") }

  # é o mesmo que o de cima, mas passo no status true or false
  # scope :by_status, -> (status) {
  #   where(status: status)
  # }


  has_many :user_projects, dependent: :destroy
  has_many :project_technologies, dependent: :destroy

  validates :name, uniqueness:
    {message: "Já existe um Projecto criado com o mesmo nome"}

end
