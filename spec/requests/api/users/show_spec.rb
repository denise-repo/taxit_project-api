# frozen_string_literal: true

require 'rails_helper'
require 'json'

RSpec.describe Api::UsersController, type: :controller do
  let(:user) { create(:user) }

  context "GET show" do
    it 'has a 200 status code' do
      get :show, params: { id: user.to_param }

      expect(response).to have_http_status(:ok)
    end

    it 'returns the correct user' do
      get :show, params: { id: user.id }

      expect(JSON.parse(response.body)["id"]).to eq(user.id)
    end
  end

end
