require 'sidekiq/web'
Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  namespace :api do
    resources :users, param: :id do
      get 'download_pdf' => 'users#download_pdf'
      delete 'delete_attachment/:attachment_id' => 'users#delete_attachment'
      member do
        get :user_projects #'/users/:id/user_projects' => 'users#user_projects'
        post :import_pdf
      end
      post :import_file, on: :collection
    end

    resources :projects
    #projects
    post '/projects/:id/user/:user_id/create_user_project' => 'projects#create_user_project'
    delete '/projects/:id/user/:user_id/delete_user_project' => 'projects#delete_user_project'

    resources :technologies

    resources :outputs do
      member do
        get :download_output
      end
      collection do
        get :generate_output
      end
    end

    resources :dashboard do
      collection do
        get :users_gender
        get :totals
        get :entry_date
      end
    end
  end
end
