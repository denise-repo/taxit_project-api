class User < ApplicationRecord
  has_many :user_projects, dependent: :destroy
  has_many :user_technologies, dependent: :destroy
  has_one_attached :file



  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone, length: { is: 9 }, numericality: { only_integer: true }, presence: true
  validate :user_unique?, on: :create

  def user_unique?
    errors.add :user, 'Já existe um utilizador criado com estes atributos' if User.find_by(first_name: first_name, last_name: last_name, phone: phone).present?
  end
end
