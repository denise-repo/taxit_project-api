require 'rails_helper'

RSpec.describe User, type: :model do

    context 'validate required fields' do
      it 'ensure first_name is present' do
        user = User.new(first_name: 'nameexample', last_name: 'lastName', phone: '911111111').save

        expect(user).to eq(true)
      end

      it 'return false if lastName is not present' do
        user = User.new(first_name: 'name2', last_name: 'lastName').save

        expect(user).to eq(false)
      end
  end
end


