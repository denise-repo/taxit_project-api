class ImportUser

  def initialize(file)
    @file = file
    @columns_translator = columns_translator
  end

  def import
    tmp_file = Tempfile.new(%w[file .xlsx], encoding: 'ascii-8bit')
    tmp_file.write(@file.file.download)
    tmp_file.close

    data = Roo::Spreadsheet.open(tmp_file)

    record_arr = []

    data.each_with_pagename do |_, sheet|
      rows = sheet.parse(@columns_translator)
      rows.each do |row|
        user_data = User.new(row)

        if user_data.valid?

          record_arr << user_data
        end
      end
    end
    User.import record_arr
  end

  private

  def columns_translator
    {
      first_name: 'Nome',
      last_name: 'Apelido',
      phone: 'Telefone',
      rank: 'Profissão',
      entry_date: 'Data de Entrada',
      email: 'Email'
    }
  end

end
