require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  let!(:user) { create(:user) }


  it "deletes user" do
    expect do
      delete :destroy, params: { id: user.to_param }

    end.to change(User.all, :count).to be(0)
  end
end
