# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Projects API', type: :request do
  let(:endpoint) do 
    "/api/projects/#{project.id}"
  end
  let(:project) { create(:project) }

  before do
    delete endpoint
  end

  describe 'delete projects/:id' do
    it 'deletes a project' do

      expect(response).to have_http_status(:ok)
    end
  end
end
