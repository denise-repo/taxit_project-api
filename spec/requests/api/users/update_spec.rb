require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  let!(:user) { create(:user) }

  let(:user_params) do
    { user: { first_name: 'José',
              last_name: 'Ferreira',
              phone: '911223456',
              rank: 'teste_profissão',
              entry_date: DateTime.now,
              email: 'jferreira@gmail.com' } }
  end

  context 'update user' do

    before do
      put :update, params: { id: user.id, user: user_params }
      user.reload
    end

    it 'responds with a status of ok' do

      expect(response).to have_http_status(200)
    end

  end
end



