class CreateImportFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :import_files do |t|
      t.string :file_name
      t.string :file_path
      t.string :file_type

      t.timestamps
    end
  end
end
