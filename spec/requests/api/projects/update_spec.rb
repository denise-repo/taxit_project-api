# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  let(:project) { create(:project) }

  let(:project_params) do
    { project: { name: "ProjectoTeste",
                 start_date: DateTime.now - 100,
                 end_date: nil,
                 status: true } }
  end

  context 'update Project' do

    before do
      put :update, params: { id: project.id, project: project_params}
      project.reload
    end


    it 'has a ok status' do

      expect(response).to have_http_status(:ok)
    end
  end
end
