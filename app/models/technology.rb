class Technology < ApplicationRecord
  has_many :user_technologies, dependent: :destroy
  has_many :project_technologies, dependent: :destroy
end
