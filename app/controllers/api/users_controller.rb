module Api
  class UsersController < ApplicationController
    attr_reader :file

    before_action :set_active_storage_host
    def set_active_storage_host
      ActiveStorage::Current.host = 'http://localhost:3000' if ActiveStorage::Current.host.blank?
      true
    end

    # GET /users
    def index
      users = User.all

      render status: :ok,
             json: users
    end

    # GET /users/:id
    def show
      user = User.find(user_id)

      render status: :ok,
             json: UserSerializer.new(user).serialize
    end

    # POST /user
    def create
      user = User.new(user_params)

      if user.save
        render status: :ok,
               json: UserSerializer.new(user).serialize
      else

        Error.create(error_message: user.errors[:user], resource: 'user')

        render status: :unprocessable_entity,
               json: { errors: user.errors }
      end
    end

    # PUT /user/:id
    def update
      user = User.find(user_id)

      if user.update!(user_params)
        render status: :ok,
               json: UserSerializer.new(user).serialize
      else
        render status: :unprocessable_entity,
               json: user.errors
      end
    end

    # DELETE user/:id
    def destroy
      user = User.find(user_id)

      if user.destroy
        render status: :ok,
               json: {message: 'Utilizador apagado com sucesso'}
      else
        render status: :unprocessable_entity,
               json: user.errors
      end
    end

    def user_projects
      user = User.includes(user_projects: :project).first

      # é o mesmo que .map {|record| record.project}
      projects = user.user_projects.map(&:project)

      render status: :ok,
             json: ProjectSerializer.new(projects).serialize_user_projects
    end

    def import_file
      instance_file = ImportFile.create(import_file_creation)
      instance_file.file.attach(io: imported_file_params.tempfile, filename: imported_file_params.original_filename)
      #instance_file.file.attach(params[:file])
      # #guardar o ficheiro
      ImportUser.new(instance_file).import

      render status: :created,
             json: ImportFileSerializer.new(instance_file).serialize
    end

    def import_pdf
      user = User.find(params[:id])

      user.file.attach(params[:file])

      render status: :ok, json: {message: 'Importado com sucesso!'}
    end

    def download_pdf
      user = User.find(params[:user_id])

      if user.nil?
        render status: :not_found
        return
      end

      #url = Rails.application.routes.url_helpers.url_for(user.file)

      render status: :ok,
             json: { url: user.file.service_url(expires_in: 10.minutes.to_i) }
    end

    def delete_attachment
      user = User.find(params[:user_id])

      user.file.purge
      user.destroy

      render status: :ok,
             json: {}
    end


    private

    def permitted_params
      params.permit(:id, user: {})
    end

    def user_id
      permitted_params[:id]
    end

    def user_params
      permitted_params[:user]
        .permit(:first_name, :last_name, :phone, :rank, :entry_date, :email, :age, :gender)
    end

    def import_file_creation
      {file_name: params[:file].original_filename,
       file_path: Rails.root.join('import', params[:file].original_filename),
       file_type: 'user_list'}
    end

    def filtered_params
      params.permit(:file_name, :import_file_id, :file)
    end

    def imported_file_params
      filtered_params[:file]
    end
  end

end
