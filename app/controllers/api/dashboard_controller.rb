require 'date'
module Api
  class DashboardController < ApplicationController


    def users_gender
      users_gender = User.group(:gender).count

      render status: :ok,
             json: users_gender.to_a
    end

    def totals
      total_projects = Project.all.count
      total_technologies = Technology.all.count
      total_users = User.all.count

      data = { "Projetos": total_projects,
               "Tecnologias": total_technologies,
               "Utilizadores": total_users }

      render status: :ok,
             json: data.to_a
    end

    def entry_date
      #entry_month = User.select(:entry_date).group(:entry_date).count
      entry_month = User.all.group_by do |user|
        user.entry_date.strftime("%B")
      end

      render status: :ok,
             json: DashboardSerializer.serialize_month(entry_month)
    end
  end
end

