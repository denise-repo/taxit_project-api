# frozen_string_literal: true

class OutputJob < ApplicationJob
  queue_as :default

  def perform(output_file, filename)
    sleep(10)
    tmp_file = Tempfile.new(filename)
    users = User.all
    projects = Project.all
    technologies = Technology.all

    OutputGenerator.new(users, projects, technologies, tmp_file.path).generate

    output_file.file.attach(io: File.open(tmp_file.path), filename: filename)
    tmp_file.close
    tmp_file.unlink
    puts '**********************************************JOB works**********************************'

  end
end
