# frozen_string_literal: true

require 'json'

class DashboardSerializer

  def self.serialize_month(entry_month)
    data = []
    entry_month.each do |record|
      data << [record[0], record[1].count]
    end
    data
  end
end
