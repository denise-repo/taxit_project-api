require 'json'

class ImportFileSerializer
  attr_reader :record

  def initialize(record)
    @record = record
  end

  def serialize
    { id: record.id,
      file_name: record.file_name,
      file_type: record.file_type,
      created_at: record.created_at.strftime('%Y-%m-%d - %H:%M'),
      updated_at: record.updated_at}
  end
end
