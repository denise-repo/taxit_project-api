class CreateErrors < ActiveRecord::Migration[6.0]
  def change
    create_table :errors do |t|
      t.string :error_message
      t.string :resource

      t.timestamps
    end
  end
end
