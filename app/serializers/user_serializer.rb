# frozen_string_literal: true

require 'json'

class UserSerializer
  def initialize(record)
    @record = record
  end

  def serialize
    {
      id: @record.id,
      first_name: @record.first_name,
      last_name: @record.last_name,
      phone: @record.phone,
      rank: @record.rank,
      entry_date: @record.entry_date,
      email: @record.email,
      age: @record.age,
      gender: @record.gender
    }
  end
end
