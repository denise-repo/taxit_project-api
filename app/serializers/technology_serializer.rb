# frozen_string_literal: true

require 'json'

class TechnologySerializer
  def initialize(record)
    @record = record
  end

  def serialize
    {
      id: @record.id,
      name: @record.name
    }
  end
end
