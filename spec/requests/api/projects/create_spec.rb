# frozen_string_literal: true

require 'rails_helper'
require 'json'

RSpec.describe Api::ProjectsController, type: :controller do
  let(:project) { create(:project) }
  
  let(:project_params) do
    { project: { name: 'Project 3000',
                 start_date: (DateTime.now - 365) } }
  end

  context 'create valid projects' do
    it 'responds with a status of ok' do
      post :create, params: project_params

      expect(response).to have_http_status(:ok)
    end
    
    it 'creates a new project' do
      expect {
        post :create, params: project_params
      }.to change { Project.count }.from(0).to(1)
    end
  end


  # context 'test projects active and inactive' do
  #   let(:project_params) { { name: 'Project 2', start_date: (DateTime.now - 20) } }
  #   before(:each) do
  #     Project.new(project_params.merge(active_project)).save
  #     Project.new(project_params.merge(active_project)).save
  #     Project.new(project_params.merge(active_project)).save
  #     Project.new(project_params).save
  #   end
  #
  #   it 'returns all active projects' do
  #     byebug
  #     expect(Project.active_project.size).to eq(3)
  #   end
  # end
end
