module Api
  class OutputsController < ApplicationController
    before_action :set_active_storage_host

    def set_active_storage_host
      ActiveStorage::Current.host = 'http://localhost:3000' if ActiveStorage::Current.host.blank?
      true
    end

    def index
      output = Output.all

      render status: :ok,
             json: OutputFileSerializer.new(output).serialize
    end

    def generate_output
      filename = 'output.xlsx'
      file_type = 'file'
      output_file = Output.create(file_name: filename, file_type: file_type)

      OutputJob.perform_later(output_file, filename)

      render status: :ok,
             json: output_file
    end

    def download_output
      output = Output.find(params[:id])

      render status: :ok,
             json: {url: output.file.service_url(expires_in: 10.minutes.to_i)}
    end
  end
end
