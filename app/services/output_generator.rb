# frozen_string_literal: true

class OutputGenerator

  def initialize(users, projects, technologies, path)
    @users = users
    @projects = projects
    @technologies = technologies
    @path = path
  end

  def generate
    p = Axlsx::Package.new

    p.workbook do |wb|
      wb.add_worksheet(name: "Utilizadores") do |sheet|
        # this is the head row
        sheet.add_row ["Primeiro Nome", "Último Nome", "Telefone", "Profissão",
                       "Data de entrada", "Email"]

        # each user is a row on spreadsheet
        @users.each do |user|
          sheet.add_row [user.first_name, user.last_name, user.phone,
                         user.rank, user.entry_date, user.email]
        end
      end
      wb.add_worksheet(name: "Projetos") do |sheet|
        # this is the head row
        sheet.add_row %w[name start_date end_date status]

        # each project is a row on spreadsheet
        @projects.each do |project|
          sheet.add_row [project.name, project.start_date, project.end_date, project.status]
        end
      end
      wb.add_worksheet(name: "Tecnologias") do |sheet|
        # this is the head row
        sheet.add_row %w[name created_at updated_at]

        # each technology is a row on spreadsheet
        @technologies.each do |technology|
          sheet.add_row [technology.name, technology.created_at, technology.updated_at]
        end
      end
    end
    p.use_shared_strings = true
    p.serialize(@path)
  end
end
