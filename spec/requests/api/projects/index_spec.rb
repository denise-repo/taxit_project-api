# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  let(:project) { create_list(:project, 3) }

  context 'GET index' do
    it 'has a 200 status code' do
      get :index

      expect(response).to have_http_status(:ok)
    end

    it 'responds with three projects' do
      get :index

      expect(project.size).to be(3)
    end
  end
end
