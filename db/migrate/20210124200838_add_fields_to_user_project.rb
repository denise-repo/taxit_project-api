class AddFieldsToUserProject < ActiveRecord::Migration[6.0]
  def change
    add_column :user_projects, :project_name, :string
    add_column :user_projects, :user_name, :string

  end
end
