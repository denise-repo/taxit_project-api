# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  #let(:endpoint) { "/api/user/index" }
  let(:user) { create_list :user, 3 }

  context 'GET index' do
    it 'has a 200 status code' do
      get :index

      expect(response).to have_http_status(:ok)
    end

    it 'return list of users' do
      get :index

      expect(user.count).to eq(3)
    end
  end
end
