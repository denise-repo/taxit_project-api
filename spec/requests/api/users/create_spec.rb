# frozen_string_literal: true

require 'rails_helper'
require 'json'

RSpec.describe Api::UsersController, type: :controller do

  #o user vem da factory
  let(:user_params) do
    { user: { first_name: 'José',
              last_name: 'Ferreira',
              phone: '911223456',
              rank: 'teste_profissão',
              entry_date: DateTime.now,
              email: 'jferreira@gmail.com' } }
  end

  context 'create a user with valid user_params' do
     it 'responds with a status of ok' do
      post :create, params: user_params

      expect(response).to have_http_status(200)
    end

     it 'persist a new user in database' do
       post :create, params: user_params

       expect(User.all.size).to equal(1)
     end
  end
end
